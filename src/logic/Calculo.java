package logic;

public class Calculo
{
  private double puntoCercano;
  private double puntoLejano;
  private double hiperfocal;
  private Camara camara;
  
  /**
   * Construye un nuevo Calculo a partir de una camara
   * con el que se realizan las operaciones necesarias.
   * 
   * @param camara 
   */
  public Calculo (Camara camara)
  {
    this.camara = camara;
  }
  
  /**
   * Calcula la hiperfocal para los valores
   * configurados en la Camara.
   */
  private void calcHiperfocal()
  {
    double N = this.camara.apertura.getNumeroF();
    double f = this.camara.getLongitudFocal();
    double c = this.camara.getCirculoConfusion();
    
    this.hiperfocal = ((f*f)/(N*c))+f;
  }
  /**
   * Calcula el punto lejano de nitidez aceptable
   * dados los valores configurados en la Camara.
   */
  private void calcPuntoLejano()
  {
		calcHiperfocal();
    double H = this.hiperfocal;
    double f = this.camara.getLongitudFocal();
    double s = this.camara.getPlanoEnfoque();
		
		this.puntoLejano = ((s*(H-f))/(H-s));
  }
  
  /**
   * Calcula el punto cercano de nitidez aceptable
   * dados los parametros configurados en la cámara.
   */
  private void calcPuntoCercano()
  {
    calcHiperfocal();
    double H = this.hiperfocal;
    double s = this.camara.getPlanoEnfoque();
    double f = this.camara.getLongitudFocal();

    this.puntoCercano = (s*(H-f))/(H+s-(2.0*f));
  }
  
  /**
   * Calcula la hiperfocal y la devuelve en mm
   * redondeados a las centenas si el parametro
	 * default es true.
	 * @param redondear True para redondear, false 
	 * para valor limpio.
   * @return 
   */
  public double getHiperfocal(boolean redondear)
  {
    calcHiperfocal();
		if(redondear)
			return Math.round(this.hiperfocal*100.0d)/100.0d;
		else
			return this.hiperfocal;
  }
  
  /**
   * Devuelve el punto lejano aceptable en foco.
   * 
	 * @param redondear Si es true, devueleve el valor redondeado
	 * a las centesimas, si es false lo devuelve limpio.
   * @return El punto lejano de foco, NaN si es infinito.
   */
  public double getPuntoLejano(boolean redondear)
  {
    calcPuntoLejano();
    if(this.puntoLejano < 0) return Double.NaN;
		
		if(redondear)
			return Math.round(this.puntoLejano*100.0d)/100.0d;
		else
			return this.puntoLejano;
  }
  
  /**
   * Devuelve el punto cercano aceptable en foco.
   * 
	 * @param redondear Si es true devuelve el valor redondeado
	 * a las centesimas, si es false devuelve el valor limpio.
   * @return El punto cercano de foco
   */
  public double getPuntoCercano(boolean redondear)
  {
    calcPuntoCercano();
		
		if(redondear)
			return Math.round(this.puntoCercano*100.0d)/100.0d;
		else
			return this.puntoCercano;
  }
}
