package logic;


public class Apertura
{
  
  public static final int TERCIO = 1;
  public static final int MEDIO = 2;
  public static final int FULL = 3;
  
  private final double [] aperturas = {1, 1.1, 1.2, 1.4, 1.6, 1.8, 2,
                                       2.2, 2.5, 2.8, 3.2, 3.5, 4, 4.5,
                                       5, 5.6, 6.3, 7.1, 8, 9, 10, 11,
                                       13, 14, 16, 18, 20, 22, 25, 28,
                                       32, 36, 40, 45};
  
	private boolean modoReal = false; //Por defecto el modo real esta desactivado.
  private double apertura = 2.8d;
  private int paso = Apertura.FULL;
  
  /**
   * Construye una nueva apertura, toma por
   * defecto como paso el valor <b>Apertura.FULL</b>
   * 
   * @param apertura
   */
  public Apertura(double apertura)
  {
    this.paso = Apertura.FULL;
    this.apertura = apertura;
  }
  
  /**
   * Construye una nueva apertura, los pasos vienen dados
   * por las siguientes constantes:
   * 
   * <ul>
   *    <li><b>Apertura.TERCIO</b>: calcula los pasos en tercios.</li>
   *    <li><b>Apertura.MEDIO</b>: calcula los pasos en medios.</li>
   *    <li><b>Apertura.FULL</b>: calcula pasos completos.</li>
   * </ul>
   * @param apertura
   * @param paso 
   */
  public Apertura(double apertura, int paso)
  {
    this.apertura = apertura;
    checkPaso(paso);
  }
  
  /**
   * Configura el tipo de salto de este objeto.
   * Si los valores no estan entre los aceptados, no se realiza 
   * ningún cambio.
   * @param paso  Las constantes de esta clase (FULL, MEDIO, TERCIO).
   */
  public void setPaso(int paso)
  {
    checkPaso(paso);
  }
  
  /**
   * Comprueba que el tipo de salto entre
   * aperturas entra dentro de los valores aceptados.
   * 
   * @param paso Las constantes de apertura FULL, MEDIO, TERCIO de esta clase.
   */
  private void checkPaso(int paso)
  {
    boolean check = paso == 1 || paso == 2 || paso == 3;
    this.paso = check ? paso : Apertura.MEDIO;//Se toma por defecto Apertura.Medio si el input no es correcto
  }
  
  /**
   * Incrementa o decrementa la apertura.
   * 
   * Los valores de salida son totalmente correctos
   * pero por convención se hacen redondeos irregulares. Para un
   * incremento de medio stop a partir de f/2.4 esta función dara
   * como resultado f/2.9 cuando por convención se usa f/2.8.
   * 
   * @param incrementando 
   */
  private void calcularN(boolean incrementando)
  {
    switch(this.paso)
    {
      case 1:
        apertura = incrementando ? apertura * Math.pow(2.0d, ((1.0d/3.0d)/2.0d)) : 1.0d/(Math.pow(2.0d, ((1.0d/3.0d)/2.0d))/apertura);
        break;
      case 2:
        apertura = incrementando ? apertura * Math.pow(2.0d, ((1.0d/2.0d)/2.0d)) : 1.0d/(Math.pow(2.0d, ((1.0d/2.0d)/2.0d))/apertura);
        break;
      case 3:
        apertura = incrementando ? apertura * Math.pow(2.0d, (1.0d/2.0d)) : 1.0d/(Math.pow(2.0d, (1.0d/2.0d))/apertura);
        break;
    }
		if(apertura < 0.3) apertura = 0.3; //Solución sucia a los valores bajando por debajo de f/0.3
  }
  /**
   * Redondea la apertura a las centesimas.
   * 
   * @param apertura
   * @return Apertura redondeada.
   */
  private double redondeoApertura(double apertura)
  {
    //TODO redondear los numeros con mas de 1 decimal a las unidades -> 7.919 = 8, si 5.6 -> 5.6 (se queda igual)
    Long salida;
    String decimal = Double.toString(Math.abs(apertura)).split("\\.")[1];//Nos quedamos con la parte decimal
    
    if(decimal.length() > 1)
    {
      salida = Math.round(apertura*100.0d);
      return salida/100.0d;
    }
    else
    {
      return apertura;
    }
  }
  
  private int buscarIndiceApertura(double numero)
  {
    for(int i = 0; i < aperturas.length; i++)
    {
      if(numero >= aperturas[i] && i+1 < aperturas.length && numero < aperturas[i+1])
      {
        return i;
      }
    }
    return -1;
  }
  
  /**
   * Calcula el numero N en base a los numeros
   * predefinidos en el array de aperturas,
   * si el numero n no se encuentra en el array, es
   * decir, o no esta o es mayor que el ultimo numero
   * se recurre a el calculo real.
   * @param incrementando 
   */
  private void calcularNFalso(boolean incrementando)
  {
    int indice = buscarIndiceApertura(this.apertura);

    if(indice != -1)
    {
      if(incrementando && indice + this.paso < this.aperturas.length)
      {
        this.apertura = this.aperturas[indice+this.paso]; 
      }
      else if (!incrementando && indice - this.paso > 0)
      {
        System.out.println("DECREMENTANDO!!!!!");
        this.apertura = this.aperturas[indice-this.paso]; 
      }
      else
      {
        calcularN(incrementando); 
      }
    }
    else
    {
      calcularN(incrementando);
    }
  }
  
  /**
   * Devuelve el numero N siguiente teniendo
   * en cuenta el tipo de incremento (Un paso completo
   * de luz, medio paso o un tercio de paso).Si este objeto
   * está configurado para dar valores por convención, los mostrará
   * salvo que se excedan los límites conocidos, en cuyo caso recurre
   * a los valores reales calculados.
   * 
   * @return La siguiente apertura.
   */
  public double getPasoSiguiente()
  {
    if(this.modoReal)
      calcularN(true);
    else
      calcularNFalso(true);
    
    return redondeoApertura(this.apertura);
  }
  
  /**
   * Devuelve el numero N anterior teniendo
   * en cuenta el tipo de decremento (Un paso completo
   * de luz, medio paso o un tercio de paso).Si este objeto
   * está configurado para dar valores por convención, los mostrará
   * salvo que se excedan los límites conocidos, en cuyo caso recurre
   * a los valores reales calculados.
   * @return La apertura anterior
   */
  public double getPasoAnterior()
  {
    if(this.apertura <= 0.3) return this.apertura;
    if(this.modoReal)
      calcularN(false);
    else
      calcularNFalso(false);
    
    return redondeoApertura(this.apertura);
  }
   
  /**
   * Devuelve la apertura actual de este objeto.
   * 
	 * @param redondeado Si es true devuelve la apertura redondeada a las centesimas
	 * si es false devuelve el double completo.
   * @return La apertura actual.
   */
  public double getNumeroF(boolean redondeado)
  {
		if(redondeado)
			return redondeoApertura(this.apertura);
		else
			return this.apertura;
  }
  
	/**
	 * Devuelve la apertura actual de este objeto.
	 * 
	 * @return La apertura actual.
	 */
	public double getNumeroF()
  {
		return this.apertura;
  }
	
  /**
   * Configura esta clase en modo real, 
   * de modo que devuelve cálculos redondeados
   * exactos y no calculos por convención y vice-versa.
   * 
   * @param modo [true] para activar, false para desactivar
   */
  public void setModoReal(boolean modo)
  {
    this.modoReal = modo;
  }
}
