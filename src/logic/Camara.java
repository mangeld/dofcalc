package logic;

public class Camara
{
  private double anchoSensor;
  private double altoSensor;
  private double longitudFocal;
  private double planoEnfoque;
  public Apertura apertura;
  private double circuloConfusion;
  private String marca, modelo;
  
  /**
   * Construye una nueva Cámara.
	 * <b>!ATENCIÓN¡</b> Todos los parámetros deben darse en milímetros.
	 * 
   * @param anchoS Ancho del sensor
   * @param altoS Alto del sensor
   * @param longFocal Longitud focal del objetivo
	 * @param planoEnfoque Distancia a la que se encuentra el plano enfocado.
   * @param apertura Apertura del objetivo. Número f. Ejemplo: 1.8 para f/1.8.
   * @param cof Circulo de confusión de la cámara.
   */
  public Camara(double anchoS, double altoS, double longFocal, double planoEnfoque, double apertura, double cof)
  {
    this.anchoSensor = anchoS;
    this.altoSensor = altoS;
    this.longitudFocal = longFocal;
    this.planoEnfoque = planoEnfoque;
    this.apertura = new Apertura(apertura);
    this.circuloConfusion = cof;
  }
  
	/**
	 * Construye una nueva Cámara.
	 * <b>¡ATENCIÓN!</b> Todos los parámetros deben darse en milímetros.
	 * 
	 * @param longitudFocal Longitud focal del objetivo.
	 * @param apertura Número f. Ejemplo: 1.8 para f/1.8.
	 * @param planoEnfoque Distancia a la que se encuentra el plano enfocado.
	 * @param cof Circulo de confusión de la cámara.
	 */
  public Camara(double longitudFocal, double apertura, double planoEnfoque, double cof)
  {
    this.longitudFocal = longitudFocal;
    this.apertura = new Apertura(apertura);
    this.planoEnfoque = planoEnfoque;
    this.circuloConfusion = cof;
  }
	
  /**
   * Construye una nueva Cámara.
   * <b>¡ATENCIÓN!</b> Todos los parámetros deben darse en milímetros.
   * @param longitudFocal Longitud focal del objetivo.
   * @param apertura Número f. Ejemplo: 1.8 para f/1.8.
   * @param cof  Circulo de confusión de la cámara.
   */
  public Camara(double longitudFocal, double apertura, double cof)
  {
    this.longitudFocal = longitudFocal;
    this.apertura = new Apertura(apertura);
    this.circuloConfusion = cof;
  }
  
  public void setPlanoEnfoque(double planoFoco)
  {
    this.planoEnfoque = planoFoco;
  }
  
  public double getAltoSensor()
  {
    return altoSensor;
  }

  public double getAnchoSensor()
  {
    return anchoSensor;
  }

  public double getCirculoConfusion()
  {
    return circuloConfusion;
  }

  public double getLongitudFocal()
  {
    return this.longitudFocal;
  }
  
  public void setLongitudFocal(double longitud)
  {
    this.longitudFocal = longitud;
  }
  
  public double getPlanoEnfoque()
	{
		return this.planoEnfoque;
	}
  
  public void setModelo(String modelo)
  {
    this.modelo = modelo;
  }
  
  public String getModelo()
  {
    return this.modelo;
  }
  
  public void setMarca(String marca)
  {
    this.marca = marca;
  }
  
  public String getMarca()
  {
    return this.marca;
  }
}
