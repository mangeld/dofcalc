package logic;

public class MathUtils
{

	/**
	 * Calcula el valor exponencial dada una entrada
	 * y un factor de crecimiento, 
	 * 
	 * @param valorInicial
	 * @param factorCrecimiento		
	 * @return 
	 */
	public static double doExp(double valorInicial, double factorCrecimiento)
	{
		return (Math.pow(factorCrecimiento, valorInicial))-1.0d;
	}
	
	public static double doInvExp(double valorExponenciado, double factorCrecimiento)
	{
		return Math.log(valorExponenciado + 1.0d) / Math.log(factorCrecimiento);
	}
	
	public static double round(double numero, int nDecimales)
	{
		assert nDecimales >= 0;
		double salida = Math.round(numero * (double)nDecimales) / (double)nDecimales;
		assert String.valueOf(Math.round(numero * (double)nDecimales) / (double)nDecimales).contains(".");
		return salida;
	}
}
