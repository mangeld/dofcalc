package logic;

public class Longitud
{
	
	private double valor; // Internamente siempre en milímetros
	
	public Longitud(double numero)
	{
		this.valor = numero;
	}
	
	public double getmm()
	{
		return this.valor;
	}
	
	public double getcm()
	{
		return this.valor / 10.0d;
	}
	
	public double getm()
	{
		return this.valor / 1000.0d;
	}
	
	public double getKm()
	{
		return this.valor / 1000000.0d;
	}
	
	public void setmm(double valor)
	{
		this.valor = valor;
	}
	
	public void setcm(double valor)
	{
		this.valor = valor * 10.0d;
	}
	
	public void setm(double valor)
	{
		this.valor = valor * 1000.0d;
	}
	
	public void setKm(double valor)
	{
		this.valor = valor * 1000000.0d;
	}
}
