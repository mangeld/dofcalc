package logic;

public class Control
{
  public static final int OK = 1;
  public static final int ARCHIVO_NO_ENCONTRADO = -1;
  public static final int DRIVER_SQLITE_NO_ENCONTRADO = -2;
  public static final int ERROR_SQL = -3;
}
