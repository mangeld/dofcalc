package data;

import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import logic.Control;

public class DB
{
  private Connection conexion = null;
  private File database;
  
  public DB()
  {
    database = new File("data/db.db");
  }
  
  public DB(File ruta)
  {
    this.database = ruta.getAbsoluteFile();
  }
  
  public DB(String ruta)
  {
    this.database = new File(ruta);
  }
  /**
   * Conecta a la base de datos especificada.
   * @return True si se ha realizado la conexion.
   * False si no se ha realizado la operación exitosamente.
   */
  public int conectar()
  {
    if(!comprobarDB(this.database.getAbsolutePath())) return Control.ARCHIVO_NO_ENCONTRADO;
    
    try
    {
      Class.forName("org.sqlite.JDBC");
      conexion = DriverManager.getConnection("jdbc:sqlite:"+database.getAbsolutePath());
    }
    catch (ClassNotFoundException ex)
    {
      return Control.DRIVER_SQLITE_NO_ENCONTRADO;
    }
    catch (SQLException ex)
    {
      return Control.ERROR_SQL;
    }
    
    return Control.OK;
  }
  
  private boolean comprobarDB(String ruta)
  {
    File archivo = new File(ruta);
    return archivo.exists();
  }
  
  /**
   * Cierra la conexión con la base de datos
   * actual.
   * @return True si se ha cerrado correctamente. False
   * si ha ocurrido algún error.
   */
  public boolean desconectar()
  {
    
    try
    {
      this.conexion.close();
    }
    catch (SQLException ex)
    {
      return false;
    }
    
    return true;
  }

  public void nuevaCamara(String marca, String modelo, double circuloConfusion)
  {
    
  }
  
  public ArrayList<String> getMarcasDisponibles()
  {
    if(conexion == null) return new ArrayList();
    
    String sql = "SELECT marcaCamara FROM camaras GROUP BY marcaCamara";

    return getColumna(sql, "marcaCamara");
  }
  
  public ArrayList<String> getModelosDisponibles(String marca)
  {
    if(conexion == null) return new ArrayList();
    
    String sentencia ="SELECT modeloCamara FROM camaras";
    if(!marca.equals("")) sentencia = sentencia.concat(" WHERE marcaCamara = \""+marca+"\"");
    return getColumna(sentencia, "modeloCamara");
  }
  
  private ArrayList<String> getColumna(String sql, String columna)
  {
    Statement s;
    ResultSet resultado;
    ArrayList<String> salida = new ArrayList<>();
    
    try
    {
      s = conexion.createStatement();
      resultado = s.executeQuery(sql);

      while(resultado.next())
      {
        salida.add(resultado.getString(columna));
      }
    }
    catch (SQLException ex)
    {
      ex.printStackTrace();
    }
    
    return salida;
  }
  
  public double getDatosCamara(String modelo)
  {
    ResultSet resultado;
    String sql = "SELECT circuloConfusion FROM camaras WHERE modeloCamara = ? ";
    PreparedStatement prep;
    
    try
    {
      prep = conexion.prepareStatement(sql);
      prep.setString(1, modelo);
      resultado = prep.executeQuery();
      if(resultado.next()) return resultado.getDouble("circuloConfusion");
    }
    catch (SQLException e)
    {
      System.err.println("Error en get datos camara: "+e.getCause());
    }
    
    return Double.NaN;
  }
 
}
