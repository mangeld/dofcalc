package view;

import data.DB;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Modality;
import javafx.stage.Stage;
import logic.Calculo;
import logic.Camara;

public class DOFCalc extends Application {
	
	private Stage stage;
  private Stage selecCamaraStage;
  private InterfazController controladorPrincipal;
  private ControladorInterfaz controladorSelecCamara;
	public static DB db;
  public static Camara camara;
  public static Calculo calculo;
  
	@Override
	public void start(Stage stage) throws Exception
  {
    FXMLLoader loader = new FXMLLoader();
    loader.setLocation(DOFCalc.class.getResource("Interfaz.fxml"));
    Parent root = loader.load();
    controladorPrincipal = loader.getController();
    controladorPrincipal.setProgramaPrincipal(this);
    Scene scene = new Scene(root);
    this.stage = new Stage();
    this.stage.setScene(scene);
		this.stage.setTitle("DOF Calculator");
		this.stage.show();
	}

	public static void main(String[] args)
  {
    DOFCalc.db = new DB("data/db.db");
		DOFCalc.camara = new Camara(50.0d, 4.0d, 0.019);
		DOFCalc.camara.setPlanoEnfoque(120000.0d);
		DOFCalc.calculo = new Calculo(DOFCalc.camara);
		launch(args);
	}
  
  public void mostrarSeleccionCamara()
  {
    try
    {
      FXMLLoader loader = new FXMLLoader();
      loader.setLocation(DOFCalc.class.getResource("PantallaSeleccionCamara.fxml"));
      Parent root = loader.load();
      controladorSelecCamara = loader.getController();
      controladorSelecCamara.setProgramaPrincipal(this);
      Scene scene = new Scene(root);
      selecCamaraStage = new Stage();
      selecCamaraStage.setScene(scene);
      selecCamaraStage.setTitle("Selecciona la camara");
      selecCamaraStage.initModality(Modality.WINDOW_MODAL);
      selecCamaraStage.initOwner(this.stage);
      selecCamaraStage.show();
    }
    catch(Exception e)
    {
      e.printStackTrace();
    }
  }
  
  public void cerrarMostrar()
  {
    selecCamaraStage.close();
    controladorPrincipal.updateValues();
		controladorPrincipal.actualizarValoresMostrados();
  }
}