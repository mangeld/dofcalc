/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package view;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.scene.text.Text;
import logic.Calculo;
import logic.Camara;

/**
 *
 * @author mike
 */
public class SeleccionarCamaraController extends ControladorInterfaz implements Initializable
{

  @FXML TreeView listaCamaras;
  TreeItem<String> items = new TreeItem<>("Camaras");
  @FXML Button boton;
  @FXML Text dofText;
  @FXML Text marca;
  @FXML Text modelo;
  @FXML Text anchoSensor;
  @FXML Text altoSensor;
  Camara camaraNueva;
  @Override public void initialize(URL location, ResourceBundle resources)
  {
    this.items.setExpanded(true);
    this.listaCamaras.setRoot(items);
    
    for(String sMarca : DOFCalc.db.getMarcasDisponibles())
    {
      TreeItem<String> marca = new TreeItem<>(sMarca);
      for(String sModelo : DOFCalc.db.getModelosDisponibles(sMarca))
      {
        marca.getChildren().add(new TreeItem<String>(sModelo));
      }
      this.items.getChildren().add(marca);
    }
    this.listaCamaras.getSelectionModel().selectedItemProperty().addListener(new ChangeListener()
    {
      @Override
      public void changed(ObservableValue observable, Object oldValue, Object newValue)
      {
        camaraSeleccionada((TreeItem<String>) newValue);
      }
    });
  }
  
  private void camaraSeleccionada(TreeItem<String> seleccion)
  {
    Double d = DOFCalc.db.getDatosCamara(seleccion.getValue());
    if(!d.equals(Double.NaN))
    {
      this.dofText.setText(Double.toString(DOFCalc.db.getDatosCamara(seleccion.getValue())));
      this.marca.setText(seleccion.getParent().getValue());
      this.modelo.setText(seleccion.getValue());
      camaraNueva = new Camara(50.0, 1.8, d);
      camaraNueva.setModelo(seleccion.getValue());
      camaraNueva.setMarca(seleccion.getParent().getValue());
			camaraNueva.setPlanoEnfoque(120000.0d);
    }
  }
  
  @FXML public void accionBoton()
  {
    if(this.camaraNueva != null)
		{
		DOFCalc.camara = this.camaraNueva;
		DOFCalc.calculo = new Calculo(DOFCalc.camara);
    main.cerrarMostrar();
		}
  }
  
}
