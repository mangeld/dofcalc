/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package view;

import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Slider;
import javafx.scene.text.Text;
import logic.Calculo;
import logic.Control;
import logic.MathUtils;
import org.controlsfx.dialog.Dialogs;

public class InterfazController extends ControladorInterfaz implements Initializable
{
  private static final double CRECIMIENTO_LONG_FOCAL = 1.075;
	private static final double CRECIMIENTO_DIST_FOCO = 1.08;
	
  @FXML Text cof;
  @FXML Text apertura;
  @FXML Text longitudFocal;
  @FXML Text puntoLejano;
  @FXML Text puntoCercano;
	@FXML Text planoFocal;
  @FXML Text puntoFoco;
  @FXML Text modeloCamaraTexto;
  @FXML Slider sliderApertura;
  @FXML Slider sliderLongitudFocal;
	@FXML Slider sliderPlanoEnfoque;
  @FXML Button botonPopup;
  @FXML ChoiceBox tipoStep;
  
  @Override public void initialize(URL url, ResourceBundle rb)
	{
    if(Control.ARCHIVO_NO_ENCONTRADO == DOFCalc.db.conectar()) 
		{
			Dialogs.create()
							.title("Error")
							.masthead("¡Error en la base de datos!")
							.message("No se encontró el archivo sqlite.")
							.showWarning();
		}
    //this.apertura.setText(Double.toString(DOFCalc.camara.apertura.getNumeroF()));
		this.tipoStep.setItems(FXCollections.observableArrayList("Tercio", "Medio","Full"));
    this.tipoStep.setValue("Full");
		this.sliderPlanoEnfoque.setMin(9.0d);
		this.sliderPlanoEnfoque.setMax(100.0d);
		this.sliderLongitudFocal.setValue(MathUtils.doInvExp(DOFCalc.camara.getLongitudFocal(), CRECIMIENTO_LONG_FOCAL));
		this.sliderPlanoEnfoque.setValue(MathUtils.doInvExp(DOFCalc.camara.getPlanoEnfoque()/1000.0d, CRECIMIENTO_DIST_FOCO));
		añadirListeners();
		actualizarValoresMostrados();
	}
	
  public void choiceBoxChanged(int i)
  {
    List o = this.tipoStep.getItems();
    if(DOFCalc.camara != null) DOFCalc.camara.apertura.setPaso(i+1);
  }
  
	public String limpiarSalida(double salida)
	{
		return String.valueOf(Math.round(salida*100.0d)/100.0d);
	}
	
	public void sliderPlanoFocoCambiado(Number numero)
	{
		double numeroExponenciado = MathUtils.doExp(numero.doubleValue(), CRECIMIENTO_DIST_FOCO)*1000.d;
		if(DOFCalc.camara != null)
		{
			DOFCalc.camara.setPlanoEnfoque(numeroExponenciado);
			actualizarValoresMostrados();
		}
	}
	
  public void sliderLongitudFocalCambiado(Number numero)
  {
    double numeroExponenciado = MathUtils.doExp(numero.doubleValue(), CRECIMIENTO_LONG_FOCAL);
		if(DOFCalc.camara != null)
		{
			DOFCalc.camara.setLongitudFocal(numeroExponenciado);
			actualizarValoresMostrados();
		}
  }
	
	public void actualizarValoresMostrados()
	{
		if(DOFCalc.camara != null)
		{
			String planoFocalT = limpiarSalida(DOFCalc.camara.getPlanoEnfoque()/1000.0d).concat(" m");
			this.planoFocal.setText(planoFocalT);
			this.puntoFoco.setText(planoFocalT);
			String longitudFocalT = String.valueOf(Math.round(DOFCalc.camara.getLongitudFocal()*100.0d)/100.0d).concat(" mm");
			this.longitudFocal.setText(longitudFocalT);
			
			Double prePuntoLejano = DOFCalc.calculo.getPuntoLejano(false);
			String puntoLejanoT;
			if(prePuntoLejano.isNaN())
				puntoLejanoT = "∞";
			else
				puntoLejanoT = limpiarSalida(prePuntoLejano/1000.0d).concat(" m");
			
			String puntoCercanoT = limpiarSalida(DOFCalc.calculo.getPuntoCercano(false)/1000.0).concat(" m");
			this.puntoCercano.setText(puntoCercanoT);
			this.puntoLejano.setText(puntoLejanoT);
		}
	}
	
  @FXML
  public void sumarApertura()
  {
    this.apertura.setText(String.valueOf(DOFCalc.camara.apertura.getPasoSiguiente()));
		actualizarValoresMostrados();
  }
  
  @FXML public void restarApertura()
  {
    this.apertura.setText(String.valueOf(DOFCalc.camara.apertura.getPasoAnterior()));
		actualizarValoresMostrados();
  }
  
  @FXML public void botonPopupAction()
  {
    this.main.mostrarSeleccionCamara();
  }
  
  public void updateValues()
  {
    this.cof.setText(Double.toString(DOFCalc.camara.getCirculoConfusion()));
    this.modeloCamaraTexto.setText(DOFCalc.camara.getModelo());
  }
	
	private void añadirListeners()
	{
		this.tipoStep.getSelectionModel().selectedIndexProperty().addListener(new ChangeListener<Number>(){
      @Override
      public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue)
      {
        choiceBoxChanged(newValue.intValue());
      }
    });
		this.sliderLongitudFocal.valueProperty().addListener(new ChangeListener<Number>(){

			@Override
			public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue)
			{
				sliderLongitudFocalCambiado(newValue);
			}
		});
		this.sliderPlanoEnfoque.valueProperty().addListener(new ChangeListener<Number>(){

			@Override
			public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
				sliderPlanoFocoCambiado(newValue);
			}
		});
	}
}
