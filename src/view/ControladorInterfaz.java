package view;

import data.DB;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.Initializable;
import logic.Calculo;
import logic.Camara;

public class ControladorInterfaz implements Initializable
{
  protected DOFCalc main;
  
  public void setProgramaPrincipal(DOFCalc programaPrincipal)
	{
		this.main = programaPrincipal;
	}
  
  @Override
  public void initialize(URL location, ResourceBundle resources)
  {
    
  }
}
