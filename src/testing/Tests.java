/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package testing;

import data.DB;
import java.math.MathContext;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import logic.Apertura;
import logic.Calculo;
import logic.Camara;

/**
 *
 * @author mike
 */
public class Tests
{
  public static void main(String [] args)
  {
    DB db = new DB();
    System.out.println("Base de datos OK: "+db.conectar());
    ArrayList<String> t = db.getMarcasDisponibles();
    for(String string : t)
    {
      System.out.println(string);
    }
  }
}
